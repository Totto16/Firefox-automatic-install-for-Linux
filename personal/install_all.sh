#!/bin/sh
#
# This will install ALL 64-bit releases for a personal installation.
#
# Firefox automatic install for Linux
# v2.9.1.0.0
#
printf -- '\n%s\n' " Now installing ALL editions of Mozilla Firefox. Please wait... ";
# Give time for user to read notice.
sleep 2; 
# Visual spacing
printf -- '\n\n\n%s\n\n' " ";
# Firefox Stable
chmod +x ./firefox.sh; ./firefox.sh;
# Firefox Beta
chmod +x ./beta.sh; ./beta.sh;
# Firefox Developer Edition
chmod +x ./developer.sh; ./developer.sh;
# Firefox Nightly
chmod +x ./nightly.sh; ./nightly.sh;
# Firefox Extended Support Release
chmod +x ./extended.sh; ./extended.sh;
# Exit notice
printf -- '%s\n' "" "" "" " Congratulations!" \
  " ALL editions of Mozilla Firfox have been installed onto your computer." \
  " They ALL will update themselves. No additional action is required." \
  " Happy browsing." "" ""
# Exit
exit 0

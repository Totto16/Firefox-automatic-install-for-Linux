#!/bin/sh
#
# This will install ALL 64-bit releases. -- To be used with Setup.sh
#
# Firefox automatic install for Linux
# v2.9.1.0.0
#
printf -- '\n%s\n' " Now installing ALL editions of Mozilla Firefox. Please wait... ";
# Give time for user to read notice.
sleep 2; 
# Visual spacing
printf -- '\n\n\n%s\n\n' " ";
# Firefox Stable
chmod +x ./personal/firefox.sh; ./personal/firefox.sh;
# Firefox Beta
chmod +x ./personal/beta.sh; ./personal/beta.sh;
# Firefox Developer Edition
chmod +x ./personal/developer.sh; ./personal/developer.sh;
# Firefox Nightly
chmod +x ./personal/nightly.sh; ./personal/nightly.sh;
# Firefox Extended Support Release
chmod +x ./personal/extended.sh; ./personal/extended.sh;
# Exit notice
printf -- '%s\n' "" "" "" " Congratulations!" \
  " ALL editions of Mozilla Firfox have been installed onto your computer." \
  " They ALL will update themselves. No additional action is required." \
  " Happy browsing." "" ""
# Exit
exit 0

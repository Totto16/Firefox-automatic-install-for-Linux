#!/bin/sh
#
# Firefox automatic install for Linux
#   v2.9.2.0.0
#
while true :
do
 clear;
 printf -- '\n%s\n' " ";
 printf -- '%s\n' "   6 4 b i t - U N I N S T A L L - C H O I C E" \
 " " \
 " 1. Remove system install for everyone (requires admin access)." \
 " 2. Remove personal install for only yourself." \
 " 3. Exit" \
 "" ""
 printf " Please enter option [1 - 3]";
 read -r opt
 case $opt in
  1) clear; printf -- '\n%s\n\n' " You selected to uninstall 64-bit system install"; 
     chmod +x ./64bit/uninstallers/uninstall_menu64.sh; ./64bit/uninstallers/uninstall_menu64.sh; exit 0 ;;

  2) clear; printf -- '\n%s\n\n' " You selected to uninstall 64-bit personal install for only yourself"; 
     chmod +x ./personal/personal_uninstall/sub_unpersonal.sh; ./personal/personal_uninstall/sub_unpersonal.sh; exit 0 ;;

  3) clear; printf -- '\n%s\n\n' " Goodbye, $USER"; exit 1;;

  *) clear;
     printf -- '\n\n%s\n' " $opt is an invaild option. Please select option between 1-3 only" \
     " Press the [enter] key to continue. . ."
     read -r enterKey;
     clear;
esac
done
